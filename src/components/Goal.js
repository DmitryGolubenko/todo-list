import React from "react"
import List from "./List"

let arrItem = [];

class Goal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {id: 0, value: '', items: []};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.removeItem = this.removeItem.bind(this);
    }

    /**
     * Обработчки ввода текста
     * @param event
     */
    handleChange(event) {
        this.setState({value: event.target.value});
    }

    /**
     * Обработчик формы
     * @param event
     */
    handleSubmit(event) {
        event.preventDefault();
        if (this.state.value.length > 0) {
            this.state.items.unshift({id: this.state.id, value: this.state.value});
            this.handleClearClick();
            this.setState(function (prevState) {
                return {
                    id: prevState.id + 1
                };
            });
            arrItem = this.state.items;
        }
    }

    /**
     * Удалить элемент
     */
    removeItem(id) {
        let nItems = this.state.items.slice();
        for (let i = 0; i < nItems.length; i++) {
            id === nItems[i].id ? nItems.splice(i, 1) : false;
        }
        this.setState({items: nItems});
    }

    /**
     * Очитстить поле ввода
     */
    handleClearClick() {
        this.setState({
            value: ''
        });
    }

    // Рендеринг
    render() {
        return (
            <div className="section-todo container col-md-6 col-md-offset-3">
                <div className="row">
                    <div className="add-goal">
                        <form onSubmit={this.handleSubmit}>
                            <div>
                                <input className="form-control" type="text" id="inputGoal" placeholder="Введите цель"
                                       value={this.state.value} onChange={this.handleChange} autoComplete="off"/>
                            </div>
                            <div className="add-btn text-right">
                                <input type="submit" className="btn btn-info" value="Отправить"/>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="row">
                    <div>
                        <List del={this.removeItem} items={this.state.items}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Goal;