import React from "react"

class ListItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {id: this.props.id};
        this.removeItem = this.removeItem.bind(this);
        this.changeItem = this.changeItem.bind(this);
    }

    changeItem() {

    }

    removeItem() {
        this.props.del(this.state.id);
    }

    render() {
        return <li id={this.state.id} className="list-goal">
            {this.props.value} <a className="del-goal" onClick={this.removeItem}><span className="glyphicon glyphicon-remove"/></a>
        </li>
    }
}

export default ListItem;
