import React from "react"
import ListItem from "./ListItem"

class List extends React.Component {
    render() {
        const items = this.props.items;
        const listItems = items.map((item) => <ListItem key={item.id} id={item.id} value={item.value}
                                                        del={this.props.del}/>);
        return <ul>{listItems}</ul>
    }
}

export default List;
